import wave
import os
import copy


def increase_speed(info):
    new_info = copy.copy(info)
    new_data = []
    needed_range = range(0, len(info.data), info.frame_width)
    for i in needed_range:
        new_data.append(info.data[i:i+1] + info.data[i+1:i+2])
    new_info.data = b''.join(new_data)
    # new_info.frame_width = 4
    new_info.name = 'incr_{}'.format(info.name)
    return new_info


def decrease_speed(info):
    new_info = copy.copy(info)
    new_data = []
    needed_range = range(0, len(info.data), info.frame_width)
    for i in needed_range:
        new_data.append(info.data[i:i+1] +
                        info.data[i+1:i+2] + info.data[i+1:i+2] +
                        info.data[i+2:i+3] +
                        info.data[i+3:i+4] + info.data[i+3:i+4])
    new_info.data = b''.join(new_data)
    new_info.name = 'decr_{}'.format(info.name)
    return new_info

# subchunk_size may be diff
# nChannels must be same? also frame_width, bits_per_sample
def connect_many(infos):
    while len(infos) != 1:
        new_info = connect(infos[0], infos[1])
        infos[0] = new_info
        infos.remove(infos[1])
    return infos[0]


def connect(info1, info2):
    new_data_frames = info1.data_frames + info2.data_frames
    new_data = b''.join(new_data_frames)
    new_data_size = bytes.__sizeof__(new_data)
    new_chunk_size = new_data_size + 36

    new_sample_rate = info1.sample_rate
    new_byte_rate = info1.byte_rate

    new_wav_info = WaveInfo(info1.chunk_id, new_chunk_size, info1.format,
                            info1.subchunk_id, info1.subchunk_size,
                            info1.audio_format, info1.nChannels,
                            new_sample_rate, new_byte_rate, info1.frame_width,
                            info1.bits_per_sample, info1.word_data,
                            new_data_size, new_data,
                            'connected_{}_{}'.format(info1.name,
                            info2.name))

    return new_wav_info


# sample_rate - частота дискретизации
def time_to_frames(duration, sample_rate):
    return duration * sample_rate


def cut_wave(info, start_time, end_time):
    frames_before_cut = round(time_to_frames(start_time, info.sample_rate))
    frames_after_cut = round(time_to_frames(end_time, info.sample_rate))
    new_data_frames = info.data_frames[frames_before_cut:frames_after_cut]

    new_data = b''.join(new_data_frames)
    new_data_size = bytes.__sizeof__(new_data)
    new_chunk_size = new_data_size + 36

    new_wav_info = WaveInfo(info.chunk_id, new_chunk_size, info.format,
                            info.subchunk_id, info.subchunk_size,
                            info.audio_format, info.nChannels,
                            info.sample_rate, info.byte_rate, info.frame_width,
                            info.bits_per_sample, info.word_data,
                            new_data_size, new_data,
                            'cut_{}'.format(info.name))
    return new_wav_info


def make_wave_info_obj(wav, name):
    info = WaveInfo(chunk_id=wav.read(4).decode('utf-8'),
                    chunk_size=int.from_bytes(wav.read(4), byteorder='little'),
                    format=wav.read(4).decode(),
                    subchunk_id=wav.read(4).decode(),
                    subchunk_size=int.from_bytes(wav.read(4),
                                                 byteorder='little'),
                    audio_format=int.from_bytes(wav.read(2),
                                                byteorder='little'),
                    nChannels=int.from_bytes(wav.read(2), byteorder='little'),
                    sample_rate=int.from_bytes(wav.read(4),
                                               byteorder='little'),
                    byte_rate=int.from_bytes(wav.read(4), byteorder='little'),
                    frame_width=int.from_bytes(wav.read(2),
                                               byteorder='little'),
                    bits_per_sample=int.from_bytes(wav.read(2),
                                                   byteorder='little'),
                    word_data=wav.read(4).decode(),
                    data_size=int.from_bytes(wav.read(4), byteorder='little'),
                    data=bytes(wav.read()), name=name)
    return info


def read_by_frames(data, frame_width, data_size):
    frame = [data[i:i + frame_width] for i in range(0, data_size, frame_width)]
    return frame


@DeprecationWarning
def change_speed_old_way(info, n=2):
    info.sample_rate *= n
    if n < 1:
        info.name = 'decr_{}'.format(info.name)
    else:
        info.name = 'incr_{}'.format(info.name)
    return info


def wave_parser(path):
    filename, file_extension = os.path.splitext(path)
    short_name = filename.split('/').pop()
    if file_extension != '.wav':
        raise NameError('Editor works only with WAV-files!')
    with open(path, mode='rb') as wav:
        info = make_wave_info_obj(wav, short_name)
    return info


class WaveInfo:
    def __init__(self, chunk_id, chunk_size, format, subchunk_id,
                 subchunk_size, audio_format, nChannels, sample_rate,
                 byte_rate, frame_width, bits_per_sample, word_data,
                 data_size, data, name):
        self.chunk_id = chunk_id
        self.chunk_size = chunk_size
        self.format = format
        self.subchunk_id = subchunk_id
        self.subchunk_size = subchunk_size
        self.audio_format = audio_format
        self.nChannels = nChannels
        self.sample_rate = sample_rate
        self.byte_rate = byte_rate
        self.frame_width = frame_width
        self.bits_per_sample = bits_per_sample
        self.word_data = word_data
        self.data_size = data_size
        self.nFrames = data_size / frame_width
        self.duration = self.nFrames / sample_rate
        self.data = data
        self.data_frames = read_by_frames(data, frame_width, data_size)
        self.name = name

    def info2file(self, filename):
        new_wav = wave.open('{}.wav'.format(filename), 'wb')
        new_wav.setnchannels(self.nChannels)
        new_wav.setsampwidth(int(self.frame_width / self.nChannels))
        new_wav.setframerate(self.sample_rate)
        new_wav.setnframes(int(self.nFrames))
        new_wav.writeframesraw(self.data)
        new_wav.close()

    def info2wav(self):
        new_wav = wave.Wave_write('hidden.wav')
        new_wav.setnchannels(self.nChannels)
        new_wav.setsampwidth(int(self.frame_width / self.nChannels))
        new_wav.setframerate(self.sample_rate)
        new_wav.setnframes(int(self.nFrames))
        new_wav.writeframesraw(self.data)
        return new_wav

    def print_data(self):
        for frame in self.data:
            print(frame)

    def print_all_info(self):
        for attr, value in self.__dict__.items():
            if value != self.data and value != self.data_frames:
                print(attr, value)

    @property
    def word_data(self):
        return self._word_data

    @word_data.setter
    def word_data(self, word_data):
        if word_data != 'data':
            raise ValueError("File is damaged")
        self._word_data = word_data

    @property
    def format(self):
        return self._format

    @format.setter
    def format(self, format):
        if format != 'WAVE':
            raise ValueError("File is damaged")
        self._format = format


if __name__ == '__main__':
    main()
