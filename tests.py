import unittest
import split_wave


class TestAudioeditor(unittest.TestCase):
    def test_wave_parser(self):
        info = split_wave.wave_parser('harry.wav')
        self.assertIsInstance(info, split_wave.WaveInfo)

        with open('text.txt', 'w', encoding='utf-8') as f:
            f.write('blablabla')
        not_wav_file = 'text.txt'
        with self.assertRaises(NameError):
            split_wave.wave_parser(not_wav_file)

    def test_connect(self):
        info1 = split_wave.wave_parser('harry.wav')
        info2 = split_wave.wave_parser('experiment.wav')
        new_wav_info = split_wave.connect(info1, info2)
        new_wav_info.info2file('test_connect')
        result_info = split_wave.wave_parser('test_connect.wav')
        self.assertEqual(info1.data_size + info2.data_size,
                         result_info.data_size)

    def test_cut_wave(self):
        info = split_wave.wave_parser('harry.wav')
        new_wav_info = split_wave.cut_wave(info, 3, 10)
        new_wav_info.info2file('cut')
        new_info = split_wave.wave_parser('cut.wav')
        self.assertEqual(new_info.duration, 10 - 3)

    def test_read_by_frames(self):
        data = b'\x00\x45\xff\xc9\x23\xcc\x12\x11'
        data_size = 8
        frame_width = 4
        result = split_wave.read_by_frames(data, frame_width, data_size)
        self.assertEqual(result, [b'\x00E\xff\xc9', b'#\xcc\x12\x11'])

        data = b'\x00\x45\xff\xc9\x23\xcc\x12\x11'
        data_size = 8
        frame_width = 2
        result = split_wave.read_by_frames(data, frame_width, data_size)
        self.assertEqual(result,
                         [b'\x00E', b'\xff\xc9', b'#\xcc', b'\x12\x11'])

    def test_info2file(self):
        info_wave1 = split_wave.wave_parser('harry.wav')
        info_wave1.info2file('harry2')
        info_wave2 = split_wave.wave_parser('harry2.wav')

        self.assertEqual(info_wave1.chunk_size, info_wave2.chunk_size)
        self.assertEqual(info_wave1.format, info_wave2.format)
        self.assertEqual(info_wave1.subchunk_id, info_wave2.subchunk_id)
        self.assertEqual(info_wave1.subchunk_size, info_wave2.subchunk_size)
        self.assertEqual(info_wave1.audio_format, info_wave2.audio_format)
        self.assertEqual(info_wave1.nChannels, info_wave2.nChannels)
        self.assertEqual(info_wave1.sample_rate, info_wave2.sample_rate)
        self.assertEqual(info_wave1.byte_rate, info_wave2.byte_rate)
        self.assertEqual(info_wave1.frame_width, info_wave2.frame_width)
        self.assertEqual(info_wave1.bits_per_sample,
                         info_wave2.bits_per_sample)
        self.assertEqual(info_wave1.word_data, info_wave2.word_data)
        self.assertEqual(info_wave1.data_size, info_wave2.data_size)
        self.assertEqual(info_wave1.data, info_wave2.data)


if __name__ == "__main__":
    unittest.main()
