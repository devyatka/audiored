import copy
import sys
import pygame
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QPushButton, QWidget, QMainWindow, QAction, \
    QApplication, \
    QInputDialog, QLCDNumber, QSlider, QVBoxLayout, QMessageBox, QDockWidget, \
    QFileDialog, QGridLayout, QLineEdit
import split_wave
from rangeslider import QRangeSlider


# тесты, тесты

# ДИЗАЙН УЕБСКИЙ

class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        self._curr_wav_info = None
        self.all_info = []
        self.wav_info2 = None
        self.dock = QDockWidget('  Current audio: Not chosen')
        self.player = None
        self.mixer = Mixer(self)
        self.slider = QRangeSlider(self)
        self.file_list = FileList(self)
        self.initUI()

    @property
    def curr_wav_info(self):
        return self._curr_wav_info

    @curr_wav_info.setter
    def curr_wav_info(self, info):
        self._curr_wav_info = info
        self.curr_wav_changed()

    def initUI(self):
        exit_action = QAction(QIcon('icons/exit.png'), 'Exit', self)
        exit_action.setShortcut('Esc')
        exit_action.setStatusTip('Exit mazafaka')
        exit_action.setToolTip('Exit')
        exit_action.triggered.connect(self.close_everything)

        cut_action = QAction(QIcon('icons/scissors.png'), 'Cut...', self)
        cut_action.setStatusTip('Cut it all')
        cut_action.setToolTip('Cut')
        cut_action.triggered.connect(self.cut_file)

        open_action = QAction(QIcon('icons/upload.png'), 'Open file...', self)
        open_action.setShortcut('Ctrl+n')
        open_action.setToolTip('Open')
        open_action.setStatusTip('Otkrivai svoiu muziku')
        open_action.triggered.connect(self.open_wav)

        save_action = QAction(QIcon('icons/save.png'), 'Save as...', self)
        save_action.setShortcut('Ctrl+s')
        save_action.setToolTip('Save')
        save_action.setStatusTip('save this shit')
        save_action.triggered.connect(self.save_wav)

        speed_action = QAction(QIcon('icons/speed.png'), 'Change speed...',
                               self)
        speed_action.setToolTip('Change speed')
        speed_action.setStatusTip('Speeeeeedyy')
        speed_action.triggered.connect(self.change_speed_dialog)

        connect_action = QAction(QIcon('icons/connect.png'),
                                 'Combine 2 files...', self)
        connect_action.setToolTip('Combine 2 files')
        connect_action.setStatusTip('Make a solyanka')
        connect_action.triggered.connect(self.combine_files)

        add_to_mixer_action = QAction(QIcon('icons/add.png'), 'Add file to '
                                                        'mixxxer', self)
        add_to_mixer_action.setToolTip('Add file to mixxxer')
        add_to_mixer_action.setStatusTip('Mixxxxxx')
        add_to_mixer_action.triggered.connect(self.add_to_mixer)

        help_action = QAction(QIcon('icons/help.png'), 'Get help', self)
        help_action.setToolTip('Help')
        help_action.setStatusTip('This is HELLp')
        help_action.triggered.connect(self.get_help)

        self.statusBar()
        self.addDockWidget(Qt.BottomDockWidgetArea, self.dock)
        self.setGeometry(430, 200, 1050, 600)
        self.setStyleSheet('background-color: 000055')
        self.setWindowTitle('AudioTroller')

        menu_bar = self.menuBar()

        file_menu = menu_bar.addMenu('File')
        tools_menu = menu_bar.addMenu('Tools')
        help_menu = menu_bar.addMenu('Help')

        file_menu.addAction(open_action)
        tools_menu.addAction(cut_action)
        tools_menu.addAction(speed_action)
        tools_menu.addAction(connect_action)
        tools_menu.addAction(add_to_mixer_action)
        file_menu.addAction(save_action)
        file_menu.addAction(exit_action)
        help_menu.addAction(help_action)

        toolbar = self.addToolBar('Commands')
        toolbar.addAction(exit_action)
        toolbar.addAction(save_action)
        toolbar.addAction(open_action)
        toolbar.addAction(cut_action)
        toolbar.addAction(speed_action)
        toolbar.addAction(connect_action)
        toolbar.addAction(add_to_mixer_action)
        toolbar.addAction(help_action)

        self.show()

    def close_everything(self):
        if len(self.all_info) != 0:
            self.ask_save_file()
        if not self.player is None:
            self.player.close()
        self.file_list.close()
        self.mixer.close()
        self.close()

    def cut_file(self):
        if not self.is_file():
            return
        self.dock.close()

        self.cut_button = QPushButton('cutcutcut', self)
        self.cut_button.clicked.connect(self.cut_on_click)
        self.cut_button.move(15, 68)
        self.cut_button.show()

        self.slider.setGeometry(100, 100, 840, 200)
        self.slider.setMax(int(self.curr_wav_info.duration))
        self.slider.show()

    def cut_on_click(self):
        new_info = split_wave.cut_wave(self.curr_wav_info,
                                       self.slider.start(), self.slider.end())
        self.all_info.append(new_info)
        self.curr_wav_info = new_info
        self.file_list.add_file(self.curr_wav_info.name)
        self.slider.close()
        self.cut_button.close()

    def change_speed_dialog(self):
        if not self.is_file():
            return
        new_info = self.curr_wav_info
        modes = ('Increase', 'Decrease')
        mode, ok = QInputDialog.getItem(self, 'Choose mode',
                                        'Available modes:', modes, 0, False)
        if ok and mode == modes[1]:
            new_info = split_wave.decrease_speed(self.curr_wav_info)
        if ok and mode == modes[0]:
            new_info = split_wave.increase_speed(self.curr_wav_info)
        message_box = QMessageBox(QMessageBox.Information,
                                  'Information', 'Speed changed')
        message_box.setText('Speed {}.'.format(
                            'increased'if mode == modes[0] else 'decreased'))
        message_box.exec()
        self.all_info.append(new_info)
        self.curr_wav_info = new_info
        self.file_list.add_file(self.curr_wav_info.name)

    def ask_save_file(self):
        save_question = QMessageBox(QMessageBox.Question, 'Save file',
                                    'Do you want to save changes?',
                                    QMessageBox.No | QMessageBox.Yes)
        save_question.setDefaultButton(QMessageBox.Yes)
        save_reply = save_question.exec()
        if save_reply == QMessageBox.Yes:
            self.save_wav()

    def open_wav(self):
        try:
            path = QFileDialog.getOpenFileName()[0]
            info = split_wave.wave_parser(path)
            self.all_info.append(copy.copy(info))
            self.curr_wav_info = info
            self.file_list.add_file(self.curr_wav_info.name)
            self.player = Player(path)

        except NameError:
            message_box = QMessageBox(QMessageBox.Warning, 'Error', 'Bad file')
            message_box.setText('Editor works only with WAV-files!')
            message_box.exec()
            return
        except ValueError:
            message_box = QMessageBox(QMessageBox.Warning, 'Error', 'Bad file')
            message_box.setText('File is damaged')
            message_box.exec()
            return

    def combine_files(self):
        if not self.is_file():
            return
        self.wav_info2 = split_wave.wave_parser(
            QFileDialog.getOpenFileName()[0])
        new_info = split_wave.connect(self.curr_wav_info, self.wav_info2)
        self.all_info.append(new_info)
        self.curr_wav_info = new_info
        self.file_list.add_file(self.curr_wav_info.name)
        message_box = QMessageBox(QMessageBox.Information, 'Information',
                                  'Your files successfully combined!')
        message_box.setText('Your files successfully combined!')
        message_box.exec()

    def save_wav(self):
        if len(self.mixer.all_info) != 0:
            self.curr_wav_info = self.mixer.get_mixed()
        if not self.is_file():
            return
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getSaveFileName(self, "QFileDialog."
                                                         "getSaveFileName()",
                                                   "new",
                                                   "WAV-files (*.wav)",
                                                   options=options)
        self.curr_wav_info.info2file(file_name)
        self.player = Player('{}.wav'.format(file_name))

    def get_help(self):
        message_box = QMessageBox(QMessageBox.Information, 'Help', 'Help')
        message_box.setText(
            'This audioeditor can cut your audio-file, change its speed and '
            'connect it to another audio-file. \n'
            '\nAttention! Editor wowks only with WAV-files! '
            '\n\nUsage: \nOn the left there is a file list to navigate '
            'between loaded files\nTo cut current file: press \'Cut\', '
            'set the slider to '
            'the preferred time, press button \'cutcutcut\'. '
            '\nTo change speed of an audio-file press \'Change speed\' '
            'and select if you want to increase speed or '
            'decrease it, then set how many times. \nTo connect your file '
            'to another audio-file, click \'Connect\' '
            'and choose another file.\nAdd files to Mixxxer which you want '
            'to connect together.\nTo save editted click '
            '\'Save as\'. ')
        message_box.exec()

    def is_file(self):
        if self.curr_wav_info is None:
            message_box = QMessageBox(QMessageBox.Warning,
                                      'Warning', 'No file')
            message_box.setText('No file chosen!')
            message_box.exec()
            return False
        return True

    def change_curr_info(self, wav_name):
        for wav in self.all_info:
            if wav.name == wav_name:
                self.curr_wav_info = wav

    def curr_wav_changed(self):
        self.removeDockWidget(self.dock)
        self.dock = QDockWidget(
            '  Current audio: {}'.format(self.curr_wav_info.name))
        self.addDockWidget(Qt.TopDockWidgetArea, self.dock)

    def add_to_mixer(self):
        if not self.is_file():
            return
        self.mixer.add_wav_info(self.curr_wav_info)


class FileList(QWidget):
    def __init__(self, parent):
        super().__init__()
        self.pa = parent
        self.buttons = []
        self.initUI()

    def initUI(self):
        self.setWindowTitle('Available audios: ')
        self.setGeometry(100, 200, 270, 600)
        layout = QGridLayout(self)
        self.setLayout(layout)
        self.show()

    def add_file(self, wav_name):
        new_button = QPushButton(wav_name, self)
        new_button.clicked.connect(lambda: self.on_click(wav_name))
        self.buttons.append(new_button)
        for button in self.buttons:
            self.layout().addWidget(button)
            button.show()

    def on_click(self, name):
        self.pa.change_curr_info(name)


class Mixer(QWidget):
    def __init__(self, parent):
        super().__init__()
        self.all_info = []
        self.pa = parent
        self.initUI()

    def initUI(self):
        self.setGeometry(1545, 200, 270, 600)
        self.setLayout(QGridLayout(self))
        self.setWindowTitle('Mixxxer')
        self.show()

    def add_wav_info(self, wav_info):
        self.all_info.append(wav_info)
        box = QLineEdit(wav_info.name, self)
        box.setEnabled(False)
        self.layout().addWidget(box)

    def get_mixed(self):
        return split_wave.connect_many(self.all_info)


class Player(QWidget):
    def __init__(self, wav):
        super().__init__()
        self.wav = wav
        pygame.mixer.init()
        self.song = pygame.mixer.Sound(self.wav)
        self.is_playing = False
        self.initUI()

    def initUI(self):
        play_button = QPushButton(QIcon('play.png'), 'Play audio', self)
        play_button.pressed.connect(self.play_audio)

        self.show()

    def play_audio(self):
        if not self.is_playing:
            self.song.play()
            self.is_playing = True
        else:
            self.song.stop()
            self.is_playing = False


# ne nuznoe no poka pust budet
class Slider(QWidget):
    close_signal = pyqtSignal()

    def __init__(self, max, wav_info):
        super().__init__()
        self.max = max
        self.wav_info = wav_info
        self.time = 1
        self.sld = None
        self.initUI()

    def initUI(self):
        self.close_signal.connect(self.close)

        lcd = QLCDNumber(self)
        self.sld = QSlider(Qt.Horizontal, self)
        vbox = QVBoxLayout()
        vbox.addWidget(lcd)
        vbox.addWidget(self.sld)

        self.setLayout(vbox)

        lcd.setMode(QLCDNumber.Dec)
        self.sld.setMaximum(self.max)
        self.sld.valueChanged.connect(lcd.display)

        button = QPushButton('cutcutcut', self)
        button.clicked.connect(self.on_click)

        self.setGeometry(300, 300, 250, 150)
        self.setWindowTitle('Set time')
        self.show()

    def on_click(self):
        self.time = self.sld.value()
        modes = ('Remove after set time', 'Remove before set time')

        mode, ok = QInputDialog.getItem(self, 'Choose mode',
                                        'Available modes:', modes, 0, False)
        if ok and mode:
            self.wav_info = split_wave.cut_wave(self.wav_info,
                                                self.time, mode == modes[0])
            message_box = QMessageBox(QMessageBox.Information, 'Information',
                                      'File cut')
            message_box.setText('Your file was successfully cut!')
            message_box.exec()
            save_question = QMessageBox(QMessageBox.Question, 'Save file',
                                        'Do you want to save changes?',
                                        QMessageBox.No | QMessageBox.Yes)
            save_question.setDefaultButton(QMessageBox.Yes)
            save_reply = save_question.exec()
            if save_reply == QMessageBox.Yes:
                options = QFileDialog.Options()
                options |= QFileDialog.DontUseNativeDialog
                file_name, _ = QFileDialog.getSaveFileName(self, "QFileDialog."
                                                                 "getSaveFileName()",
                                                           "new",
                                                           "WAV-files (*.wav)",
                                                           options=options)
                self.wav_info.info2file(file_name)
            self.close_signal.emit()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = Window()
    sys.exit(app.exec_())
