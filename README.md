Audio-Editor

Description:
This audioeditor can cut your audio-file, change its speed and connect it to another audio-file. 

Requirements:
python3

Contents:
editor.py - application to run
split_wave.py - logic of the application (auxiliary file)
rangeslider.py - python class QRangeSlider (auxiliary file)
tests.py - tests for the application
icons - a folder with needed icons for the program

Usage:
To cut file: press 'Cut', set the slider to the preferred time, press button 'cutcutcut'.
To change speed of an audio-file press 'Change speed' and select if you want to increase speed ordecrease it, then set how many times. 
To connect your file to another audio-file, click 'Connect' and choose another file. 
Add files to Mixxxer which you want to connect together.
To save editted click 'Save as'.

HotKeys:
Esc 		| close the Editor
Ctrl + o 	| open file
Ctrl + s 	| save file as
